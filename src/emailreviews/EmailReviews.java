/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package emailreviews;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

/**
 *
 * @author Max
 * @repository https://bitbucket.org/mjones_gnv/compile-reviews-script
 */
public class EmailReviews {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String username;
        String password;
        String route = "products/mine";
        String query = "";
        
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter appFigures username:");
        username = scanner.next();
        
        System.out.print("Enter appFigures password:");
        password = scanner.next();
        
        String productsJSON = new GetJSON(username, password, route, query).httpsRequest();
        
        Calendar cal = Calendar.getInstance();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        
        cal.add(Calendar.DATE, -1);
        String end = dateFormat.format(cal.getTime());
        
        cal.add(Calendar.DATE, -6);
        String start = dateFormat.format(cal.getTime());
        
        String allInput = "";
        
        try {
            File dateInputFile = new File("last_review_check.json");
            
            if (dateInputFile.exists()) {
                BufferedReader br = new BufferedReader(new FileReader(dateInputFile));
            
                String sCurrentLine;
                while ((sCurrentLine = br.readLine()) != null) {
                    allInput += sCurrentLine;
                }
                
                if (!allInput.equals("")) {
                    if (allInput.compareTo(end) < 0) {
                        start = allInput;
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        route = "reviews";
        query = "start=" + start + "&end=" + end;
        String reviewsJSON = new GetJSON(username, password, route, query).httpsRequest();
        
        JSONObject products = (JSONObject)JSONValue.parse(productsJSON);
        JSONObject reviewsInfo = (JSONObject)JSONValue.parse(reviewsJSON);
        JSONArray reviews = (JSONArray)reviewsInfo.get("reviews");
        
        HashMap<String, HashMap<Long, String>> stores = new HashMap<>();
        
        for (Object o : reviews) {
            JSONObject current = (JSONObject) o;
            JSONObject currentProduct = (JSONObject) products.get(current.get("product").toString());
            
            HashMap<Long, String> store = stores.get((String) currentProduct.get("storefront"));
            
            //need to use calendar and SimpleDateFormat to parse out the date
            String html = "<p>";
            
            Pattern p = Pattern.compile("[0-9]{4}-[0-9]{2}-[0-9]{2}");
            Matcher m = p.matcher((String) current.get("date"));
            if (m.find()) {
                html += m.group(0) + " - ";
            }
            
            int numStars = (int) Double.parseDouble((String) current.get("stars"));
            for (int i = 0; i < numStars; i++) {
                html += "*";
            }
            
            html += " - <b>" + (String) current.get("title") + "</b> - " + (String) current.get("review") + "</p>";
            
            if (store == null) {
                HashMap<Long, String> product = new HashMap<>();
                product.put((long) current.get("product"), html);
                stores.put((String)currentProduct.get("storefront"), product);
            } else {
                String reviewsHTML = store.get((long) current.get("product"));
                
                if (reviewsHTML == null) {
                    store.put((Long) current.get("product"), html);
                } else {
                    store.put((Long) current.get("product"), reviewsHTML + html);
                }
            }
        }
        
        //loop through stores HashMap and create HTML email content
        String emailHTML = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN'"
                + "'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'>"
                + "<head><title>Reviews</title></head>"
                + "<body><h1 style='text-align:center'>";
        
        String timeframe = "Reviews " + start + " to " + end;
        
        emailHTML += timeframe + "</h1>";
        
        for (Entry<String, HashMap<Long, String>> storeEntry : stores.entrySet()) {
            emailHTML += "<h2 style='padding-left:10%'>" + storeEntry.getKey() + "</h2>";
            
            for (Entry<Long, String> productEntry : storeEntry.getValue().entrySet()) {
                JSONObject productInfo = (JSONObject) products.get(productEntry.getKey().toString());
                emailHTML += "<h3 style='padding-left:20%;text-decoration:underline'>" + productInfo.get("name") + "</h3>";
                emailHTML += "<div style='padding-left:25%'>" + productEntry.getValue() + "</div>";
            }
        }
        
        emailHTML += "</body></html>";
        
        System.out.print("Enter your gmail username (example@atmoapps.com):");
        username = scanner.next();
        
        System.out.print("Enter your gmail password:");
        password = scanner.next();
        
        
        //send HTML email
        new SendEmail(username, password, timeframe, emailHTML).execute();
        
        //set last_review_check.json to today
        try {
            File file = new File("last_review_check.json");
        
            if (!file.exists()) {
                file.createNewFile();
            }
        
            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            
            cal.add(Calendar.DATE, 7);
            
            bw.write(dateFormat.format(cal.getTime()));
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
