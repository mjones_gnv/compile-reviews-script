/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package emailreviews;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import org.apache.commons.codec.binary.Base64;
/**
 *
 * @author Max
 * @repository https://bitbucket.org/mjones_gnv/compile-reviews-script
 */
public class GetJSON {
    final private String base_url = "https://api.appfigures.com/v2/";
    final private String client_key = "0d4c90500cca4531984066853f0b2ca9";
    final private String username;
    final private String password;
    final private String route;
    final private String query;
    
    public GetJSON(String u, String pw, String r, String q) {
        this.username = u;
        this.password = pw;
        this.route = r;
        this.query = q;
    }
    
    public String httpsRequest() {
        String response = "";
        
        try {
            String fullUrl = base_url + route + "?client_key=" + client_key;
            
            if (query != null) {
                fullUrl += "&" + query;
            }
            
            URL url = new URL(fullUrl);
            
            String encoding = new String(Base64.encodeBase64((username + ":" + password).getBytes()));

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty  ("Authorization", "Basic " + encoding);
            
            InputStream content = (InputStream)connection.getInputStream();
            BufferedReader in = new BufferedReader (new InputStreamReader (content));
            
            String line;
            while ((line = in.readLine()) != null) {
                response += line;
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        
        return response;
    }
}
